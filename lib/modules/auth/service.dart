import 'dart:developer' show log;

import 'package:automator/config/backend/service.dart' show BackendService;
import 'package:supabase_flutter/supabase_flutter.dart' show User, Provider;

/// A service that provides access to Supabase authentication
class AuthService {
  late final _provider = BackendService().client.auth;

  /// Returns the current user
  User? get user => _provider.user();

  /// Registers a new user with the given [email] and [password]
  Future<void> register({
    required String email,
    required String password,
  }) async {
    try {
      await _provider.signUp(email, password);
      log('Logged in with email and password');
      log(user?.toString() ?? '');
    } catch (e) {
      throw Exception('Failed to login with email and password, $e');
    }
  }

  /// Logs in the user with the given [email] and [password]
  Future<void> login({
    required String email,
    required String password,
    Provider? provider,
  }) async {
    try {
      if (provider == null) {
        await _provider.signIn(email: email, password: password);
      } else {
        await _provider.signIn(
          email: email,
          password: password,
          provider: provider,
        );
      }
      log('Logged in with email and password');
      log(user?.toString() ?? '');
    } catch (e) {
      throw Exception('Failed to login with email and password, $e');
    }
  }

  Future<void> logout() async {
    final user = this.user;
    if (user != null) {
      await _provider.signOut();
      log('Logged out');
      log(user.toString());
    } else {
      throw Exception('No user to log out');
    }
  }
}
