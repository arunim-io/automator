import 'package:flutter/material.dart';

import '../widgets/forms/login.dart';

class LoginView extends StatelessWidget {
  const LoginView({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.teal[900],
      resizeToAvoidBottomInset: true,
      body: SingleChildScrollView(
        child: Center(
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              Container(
                margin: const EdgeInsets.symmetric(vertical: 50),
                child: const Image(
                  image: AssetImage('assets/images/icon.png'),
                  height: 100,
                ),
              ),
              Container(
                margin: const EdgeInsets.symmetric(horizontal: 20),
                padding: const EdgeInsets.symmetric(
                  horizontal: 30,
                  vertical: 45,
                ),
                child: const LoginForm(),
                color: Colors.white,
              ),
            ],
          ),
        ),
      ),
    );
  }
}
