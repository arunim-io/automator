import 'package:auto_route/annotations.dart';

import 'constants.dart';
import 'views.dart';

@MaterialAutoRouter(
  replaceInRouteName: 'Page,Route',
  routes: <AutoRoute>[
    AutoRoute(
      page: HomeView,
      initial: true,
      path: Routes.home,
    ),
    AutoRoute(
      page: LoginView,
      path: Routes.login,
    ),
  ],
)

/// App Router class
class $AppRouter {}
