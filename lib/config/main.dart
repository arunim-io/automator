import 'package:automator/config/routes/main.gr.dart';
import 'package:flutter/material.dart';

/// App Configuration
class Config extends StatelessWidget {
  Config({Key? key}) : super(key: key);
  final _appRouter = AppRouter();
  @override
  Widget build(BuildContext context) {
    return MaterialApp.router(
      title: 'Automator',
      theme: ThemeData(
        primarySwatch: Colors.green,
      ),
      routerDelegate: _appRouter.delegate(),
      routeInformationParser: _appRouter.defaultRouteParser(),
    );
  }
}
